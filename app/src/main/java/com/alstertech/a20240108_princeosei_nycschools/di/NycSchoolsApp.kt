package com.alstertech.a20240108_princeosei_nycschools.di

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class NycSchoolsApp: Application() {

}