package com.alstertech.a20240108_princeosei_nycschools.di.modules

import android.content.Context
import com.alstertech.a20240108_princeosei_nycschools.network.NetworkApi
import com.alstertech.a20240108_princeosei_nycschools.utils.CommonUtils
import com.alstertech.a20240108_princeosei_nycschools.utils.Constants.Companion.BASE_URL
import com.alstertech.a20240108_princeosei_nycschools.utils.Constants.Companion.USER_AGENT
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

/**
 * NetworkModule.kt
 * Module to hold all network/API related Dependency Injections
 */
@Module
@InstallIn(SingletonComponent::class)
object NetworkModule {

    @Singleton
    @Provides
    fun providesOkHttpClient(
        interceptor: HttpLoggingInterceptor,
        userAgent: String): OkHttpClient {

        return OkHttpClient.Builder()
            .addInterceptor { chain ->
                chain.proceed(chain.request().newBuilder().header(USER_AGENT, userAgent).build())
            }
            .addInterceptor(interceptor)    // Could add token interceptors here if needed for authentication
            .build()
    }
    @Singleton
    @Provides
    fun providesLogging(): HttpLoggingInterceptor =
        HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY)

    @Singleton
    @Provides
    fun userAgent(@ApplicationContext context: Context): String = CommonUtils.getCustomUserAgent(context)

    @Singleton
    @Provides
    fun provideRetrofitInstance(okHttpClient: OkHttpClient): Retrofit =
        Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .client(okHttpClient)
            .build()

    @Singleton
    @Provides
    fun provideNetworkApi(retrofit: Retrofit): NetworkApi = retrofit.create(NetworkApi::class.java)

}