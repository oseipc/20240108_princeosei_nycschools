package com.alstertech.a20240108_princeosei_nycschools.models

import com.alstertech.a20240108_princeosei_nycschools.utils.Constants

data class SchoolDataModel(
    val dbn: String,
    val boro: String?,
    val schoolName: String,
    val overviewParagraph: String?,
    val phoneNumber: String,
    val schoolEmail: String,
    val website: String,
    val primaryAddressLine1: String?,
    val city: String?,
    val zip: Int?,
    val stateCode: String?,
    var numOfSatTestTakers: String = Constants.VALUE_NOT_AVAILABLE,
    var satCriticalReadingAvgScore: String = Constants.VALUE_NOT_AVAILABLE,
    var satMathAvgScore: String = Constants.VALUE_NOT_AVAILABLE,
    var satWritingAvgScore: String = Constants.VALUE_NOT_AVAILABLE
)