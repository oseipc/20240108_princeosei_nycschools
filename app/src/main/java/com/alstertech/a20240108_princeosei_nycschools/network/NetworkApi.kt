package com.alstertech.a20240108_princeosei_nycschools.network

import retrofit2.Response
import retrofit2.http.GET

interface NetworkApi {
    @GET("resource/s3k6-pzi2.json")
    suspend fun fetchSchoolRecords(): Response<List<SchoolResponseModel>>

    @GET("resource/f9bf-2cp4.json")
    suspend fun fetchSatRecords(): Response<List<SatScoresResponseModel>>
}