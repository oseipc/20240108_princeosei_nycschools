package com.alstertech.a20240108_princeosei_nycschools.network

import com.google.gson.annotations.SerializedName

data class SchoolResponseModel (
    val dbn: String?,
    val boro: String?,
    @SerializedName("school_name")
    val schoolName: String?,
    @SerializedName("overview_paragraph")
    val overviewParagraph: String?,
    @SerializedName("phone_number")
    val phoneNumber: String?,
    @SerializedName("school_email")
    val schoolEmail: String?,
    val website: String?,
    @SerializedName("primary_address_line_1")
    val primaryAddressLine1: String?,
    val city: String?,
    val zip: Int?,
    @SerializedName("state_code")
    val stateCode: String?
)

data class SatScoresResponseModel (
    val dbn: String?,
    @SerializedName("school_name")
    val schoolName: String?,
    @SerializedName("num_of_sat_test_takers")
    val numOfSatTestTakers: String?,
    @SerializedName("sat_critical_reading_avg_score")
    val satCriticalReadingAvgScore: String?,
    @SerializedName("sat_math_avg_score")
    val satMathAvgScore: String?,
    @SerializedName("sat_writing_avg_score")
    val satWritingAvgScore: String?
)