package com.alstertech.a20240108_princeosei_nycschools.repos

import android.util.Log
import com.alstertech.a20240108_princeosei_nycschools.network.NetworkApi
import com.alstertech.a20240108_princeosei_nycschools.utils.DataState
import retrofit2.Response
import javax.inject.Inject

class SchoolsRepository @Inject constructor(
    private val networkApi: NetworkApi
) {
    private val tagName = SchoolsRepository::class.simpleName

    suspend fun fetchSchoolsList(): DataState<Any> {
        return processDataState(networkApi.fetchSchoolRecords())
    }
    suspend fun fetchSatScores(): DataState<Any> {
        return processDataState(networkApi.fetchSatRecords())
    }


    private fun processDataState(result: Response<*>): DataState<Any> {
        try {
            val responseBody = result.body()
            return when {
                responseBody != null -> {
                    Log.d(tagName, "success body - $responseBody")
                    DataState.Success(responseBody)
                }
                else -> {
                    val errorBody = result.errorBody()?.string()
                    Log.d(tagName, "error body - $errorBody")
                    if (errorBody != null) {
                        DataState.Failure(errorBody)
                    } else {
                        Log.e(tagName, "Neither response body or error body found")
                        throw Exception("Oops! An error occurred")
                    }
                }
            }
        } catch (e: Exception) {
            return DataState.Error(e)
        }
    }
}