package com.alstertech.a20240108_princeosei_nycschools.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import com.alstertech.a20240108_princeosei_nycschools.databinding.FragmentSatDetailsBinding
import com.alstertech.a20240108_princeosei_nycschools.viewmodels.SchoolsVM
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class SatDetailsFragment : Fragment() {

    private val schoolsVM: SchoolsVM by activityViewModels()
    private lateinit var binding: FragmentSatDetailsBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {

        binding = FragmentSatDetailsBinding.inflate(inflater, container, false)
        binding.model = schoolsVM.selectedSchoolsListItem.get()
        binding.vm = schoolsVM

        return binding.root
    }

}