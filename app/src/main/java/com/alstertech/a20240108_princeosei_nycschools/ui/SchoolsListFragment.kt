package com.alstertech.a20240108_princeosei_nycschools.ui

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import com.alstertech.a20240108_princeosei_nycschools.R
import com.alstertech.a20240108_princeosei_nycschools.databinding.FragmentSchoolsListBinding
import com.alstertech.a20240108_princeosei_nycschools.viewmodels.SchoolsVM
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class SchoolsListFragment : Fragment() {

    private val schoolsVM: SchoolsVM by activityViewModels()
    private lateinit var binding: FragmentSchoolsListBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        binding =  FragmentSchoolsListBinding.inflate(inflater, container, false)
        binding.vm = schoolsVM


        return binding.root
    }

    override fun onResume() {
        super.onResume()
        if (!schoolsVM.isSchoolsFetched) {
            schoolsVM.fetchSatScores(requireContext())
        }
    }
}