package com.alstertech.a20240108_princeosei_nycschools.ui.adapters

import android.view.View
import com.alstertech.a20240108_princeosei_nycschools.models.SchoolDataModel

interface SchoolItemListener {
    fun onSchoolItemClicked(view: View, school: SchoolDataModel)
}