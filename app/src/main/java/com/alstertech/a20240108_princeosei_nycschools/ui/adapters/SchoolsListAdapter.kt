package com.alstertech.a20240108_princeosei_nycschools.ui.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.alstertech.a20240108_princeosei_nycschools.databinding.ListItemSchoolBinding
import com.alstertech.a20240108_princeosei_nycschools.models.SchoolDataModel

/**
 * SchoolsListAdapter.kt
 * Recycler adapter to manage schools list
 * If project contains multiple lists, a generic recycler adapter would be used to
 * replace this implementation in order to reduce boilerplate code
 */
class SchoolsListAdapter(private val schoolItemListener: SchoolItemListener): RecyclerView.Adapter<SchoolsListAdapter.ViewHolder>() {

    private val diffSchoolsList = AsyncListDiffer(this, object: DiffUtil.ItemCallback<SchoolDataModel>() {
        override fun areItemsTheSame(oldItem: SchoolDataModel, newItem: SchoolDataModel): Boolean {
            return oldItem.dbn == newItem.dbn && newItem.schoolName == oldItem.schoolName
        }

        override fun areContentsTheSame(oldItem: SchoolDataModel, newItem: SchoolDataModel): Boolean {
            return oldItem.boro == newItem.boro &&
                    oldItem.primaryAddressLine1 == newItem.primaryAddressLine1 &&
                    oldItem.city == newItem.city &&
                    oldItem.zip == newItem.zip &&
                    oldItem.stateCode == newItem.stateCode
        }

    })

    class ViewHolder(val binding: ListItemSchoolBinding, private val schoolItemListener: SchoolItemListener): RecyclerView.ViewHolder(binding.root) {
        fun onBind(modelItem: SchoolDataModel) {
            binding.model = modelItem
            binding.root.setOnClickListener {  }
            binding.root.setOnClickListener{
                schoolItemListener.onSchoolItemClicked(it, modelItem)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = ListItemSchoolBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(binding, schoolItemListener)
    }

    override fun getItemCount(): Int {
        return diffSchoolsList.currentList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.onBind(diffSchoolsList.currentList[position])
    }

    fun submitSchoolsList(newSchoolsList: List<SchoolDataModel>) {
        diffSchoolsList.submitList(newSchoolsList)
    }

}