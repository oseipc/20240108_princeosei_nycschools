package com.alstertech.a20240108_princeosei_nycschools.utils

import android.content.res.Resources.Theme
import android.graphics.drawable.Drawable
import android.view.ContextThemeWrapper
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.alstertech.a20240108_princeosei_nycschools.R
import com.google.android.material.textview.MaterialTextView

object BindingAdapterUtil {

    @JvmStatic
    @BindingAdapter("recyclerAdapter")
    fun setRecyclerAdapter(recyclerView: RecyclerView, adapter: RecyclerView.Adapter<*>?) {
        recyclerView.layoutManager =
            LinearLayoutManager(recyclerView.context, LinearLayoutManager.VERTICAL, false)
        recyclerView.adapter = adapter
    }

    @JvmStatic
    @BindingAdapter("dividerLine")
    fun setDividerLine(view: RecyclerView, dividerDrawable: Drawable) {
        val divider = ListDividerItemDecoration(dividerDrawable)
        view.addItemDecoration(divider)
    }

    @JvmStatic
    @BindingAdapter("circleBackground")
    fun setCircleBackground(view: MaterialTextView, boro: String) {
        val backgroundResId = when (boro) {
            "K" -> {
                R.drawable.circle_grey
            }
            "M" -> {
                R.drawable.circle_blue
            }
            "Q" -> {
                R.drawable.circle_pink
            }
            "R" -> {
                R.drawable.circle_purple
            }
            "X" -> {
                R.drawable.circle_teal
            }
            else -> {
                R.drawable.circle_brown
            }
        }
        view.background = ContextCompat.getDrawable(view.context, backgroundResId)
    }

    @JvmStatic
    @BindingAdapter("streetAddress", "city", "stateCode", "zip")
    fun setAddress(view: MaterialTextView, streetAddress: String?, city: String?, stateCode: String?, zip: Int) {
        view.text = view.context.getString(R.string.format_address, streetAddress, city, stateCode, zip)
    }

}