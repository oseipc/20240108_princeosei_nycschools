package com.alstertech.a20240108_princeosei_nycschools.utils

import android.app.Activity
import android.content.Context
import android.content.ContextWrapper
import android.os.Build
import android.text.TextUtils
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.FragmentManager
import okhttp3.internal.userAgent

object CommonUtils {

    fun getCustomUserAgent(context: Context): String = "${getApplicationName(context)}/" +
            "1.0 " +
            "(${context.packageName}; " +
            "build:1 " +
            "Android SDK ${Build.VERSION.SDK_INT}) " +
            "$userAgent " +
            getDeviceName()

    private fun getApplicationName(context: Context): String {
        val applicationInfo = context.applicationInfo
        val stringId = applicationInfo.labelRes
        return if (stringId == 0) applicationInfo.nonLocalizedLabel.toString() else context.getString(
            stringId
        )
    }

    private fun getDeviceName(): String {
        val manufacturer = Build.MANUFACTURER
        val model = Build.MODEL
        return if (model.startsWith(manufacturer)) {
            capitalize(model)
        } else capitalize(manufacturer) + " " + model
    }

    private fun capitalize(str: String): String {
        if (TextUtils.isEmpty(str)) {
            return str
        }
        val arr = str.toCharArray()
        var capitalizeNext = true
        val phrase = StringBuilder()
        for (c in arr) {
            if (capitalizeNext && Character.isLetter(c)) {
                phrase.append(Character.toUpperCase(c))
                capitalizeNext = false
                continue
            } else if (Character.isWhitespace(c)) {
                capitalizeNext = true
            }
            phrase.append(c)
        }
        return phrase.toString()
    }

    fun scanForActivity(context: Context?): Activity? {
        if (context == null) {
            return null
        } else if (context is Activity) {
            return context
        } else if (context is ContextWrapper) {
            return scanForActivity(context.baseContext)
        }
        return null
    }

    fun getSupportFragmentManager(context: Context?): FragmentManager? {
        val activity = scanForActivity(context) ?: return null
        return (activity as FragmentActivity).supportFragmentManager
    }
}