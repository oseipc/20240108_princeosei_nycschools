package com.alstertech.a20240108_princeosei_nycschools.utils

class Constants {
    companion object {
        const val BASE_URL = "https://data.cityofnewyork.us/"
        const val USER_AGENT = "User-Agent"
        const val VALUE_NOT_AVAILABLE = "N/A"
    }
}