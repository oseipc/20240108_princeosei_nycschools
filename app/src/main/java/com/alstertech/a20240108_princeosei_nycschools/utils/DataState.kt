package com.alstertech.a20240108_princeosei_nycschools.utils


sealed class DataState<out R> {
    data class Success<out T>(val data: T): DataState<T>()
    data class Failure<out T>(val data: T): DataState<T>()
    data class Error<out T>(val exception: T): DataState<T>()
}