package com.alstertech.a20240108_princeosei_nycschools.viewmodels

import android.content.Context
import android.util.Log
import android.view.View
import androidx.annotation.VisibleForTesting
import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.navigation.findNavController
import com.alstertech.a20240108_princeosei_nycschools.R
import com.alstertech.a20240108_princeosei_nycschools.models.SchoolDataModel
import com.alstertech.a20240108_princeosei_nycschools.network.SatScoresResponseModel
import com.alstertech.a20240108_princeosei_nycschools.network.SchoolResponseModel
import com.alstertech.a20240108_princeosei_nycschools.repos.SchoolsRepository
import com.alstertech.a20240108_princeosei_nycschools.ui.SchoolsListFragmentDirections
import com.alstertech.a20240108_princeosei_nycschools.ui.adapters.SchoolItemListener
import com.alstertech.a20240108_princeosei_nycschools.ui.adapters.SchoolsListAdapter
import com.alstertech.a20240108_princeosei_nycschools.utils.Constants.Companion.VALUE_NOT_AVAILABLE
import com.alstertech.a20240108_princeosei_nycschools.utils.DataState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class SchoolsVM @Inject constructor(
    private val schoolsRepository: SchoolsRepository
): ViewModel(), SchoolItemListener {
    private val tagName = SchoolsVM::class.simpleName

    val showLoadingDataIndicator = ObservableBoolean(true)
    val showDataNotFoundMessage = ObservableBoolean(false)
    var showSchoolsList = ObservableBoolean(false)

    val loadingDataStatus = ObservableField<String>()
    val errorMessage = ObservableField<String>()

    var schoolsListAdapter = ObservableField<SchoolsListAdapter>()
    var selectedSchoolsListItem = ObservableField<SchoolDataModel>()

    var satScoresList = mutableListOf<SatScoresResponseModel>()
    var isSchoolsFetched = false

    fun initiateSchoolsListAdapter(context: Context){
        viewModelScope.launch {
            loadingDataStatus.set(context.getString(R.string.text_fetching_schools))

            when (val response = schoolsRepository.fetchSchoolsList()) {
                is DataState.Success -> {
                    loadingDataStatus.set(context.getString(R.string.text_cleaning_data))
                    val schoolsListResponse = response.data as List<*>
                    val schoolsList = mutableListOf<SchoolDataModel>()

                    for (school in schoolsListResponse) {
                        if ((school as SchoolResponseModel).dbn != null && school.schoolName != null ) {
                            schoolsList.add(getSchoolDataModel(school))
                        }
                    }
                    if (schoolsList.isNotEmpty()) {
                        isSchoolsFetched = true
                        val adapter = SchoolsListAdapter(this@SchoolsVM)
                        adapter.submitSchoolsList(schoolsList)
                        schoolsListAdapter.set(adapter)

                        showLoadingDataIndicator.set(false)
                        showDataNotFoundMessage.set(false)
                        showSchoolsList.set(true)
                    } else {
                        showErrorMessage(context.getString(R.string.text_data_not_found))
                    }

                }
                is DataState.Failure -> {
                    Log.e(tagName, "fetch schools list failed")
                    showErrorMessage(context.getString(R.string.text_error_occurred))
                }
                is DataState.Error -> {
                    Log.e(tagName, "exception occurred while fetching schools list")
                    showErrorMessage(context.getString(R.string.text_error_occurred))
                }
            }
        }
    }

    fun showErrorMessage(errorMsg: String) {
        showSchoolsList.set(false)
        showLoadingDataIndicator.set(false)
        showDataNotFoundMessage.set(true)
        errorMessage.set(errorMsg)
    }

    fun fetchSatScores(context: Context){
        viewModelScope.launch {
            loadingDataStatus.set(context.getString(R.string.text_fetching_sat_scores))

            when (val response = schoolsRepository.fetchSatScores()) {
                is DataState.Success -> {
                    satScoresList.addAll(response.data as List<SatScoresResponseModel>)
                    satScoresList.sortBy { it.dbn }
                    initiateSchoolsListAdapter(context)
                }
                is DataState.Failure -> {
                    Log.e(tagName, "fetch sat scores failed")
                    showErrorMessage(context.getString(R.string.text_error_occurred))
                }
                is DataState.Error -> {
                    Log.e(tagName, "exception occurred while fetching sat scores")
                    showErrorMessage(context.getString(R.string.text_error_occurred))
                }
            }
        }
    }

    private fun getSchoolDataModel(schoolResponseModel: SchoolResponseModel): SchoolDataModel {
        val schoolDataModel = SchoolDataModel(
            schoolResponseModel.dbn!!, // already check for nullable before this call
            schoolResponseModel.boro,
            schoolResponseModel.schoolName!!, // already checked for nullable before this call
            schoolResponseModel.overviewParagraph?:VALUE_NOT_AVAILABLE,

            schoolResponseModel.phoneNumber?:VALUE_NOT_AVAILABLE,
            schoolResponseModel.schoolEmail?:VALUE_NOT_AVAILABLE,
            schoolResponseModel.website?:VALUE_NOT_AVAILABLE,
            schoolResponseModel.primaryAddressLine1,

            schoolResponseModel.city,
            schoolResponseModel.zip,
            schoolResponseModel.stateCode
        )
        val scoreIndex = satScoresList.binarySearch { String.CASE_INSENSITIVE_ORDER.compare(it.dbn, schoolResponseModel.dbn) }

        if (scoreIndex >= 0) {
            val satScore = satScoresList[scoreIndex]

            schoolDataModel.numOfSatTestTakers = satScore.numOfSatTestTakers?:VALUE_NOT_AVAILABLE
            schoolDataModel.satCriticalReadingAvgScore = satScore.satCriticalReadingAvgScore?:VALUE_NOT_AVAILABLE
            schoolDataModel.satMathAvgScore = satScore.satMathAvgScore?:VALUE_NOT_AVAILABLE
            schoolDataModel.satWritingAvgScore = satScore.satWritingAvgScore?:VALUE_NOT_AVAILABLE
        }
        return schoolDataModel
    }

    override fun onSchoolItemClicked(view: View, school: SchoolDataModel) {
        selectedSchoolsListItem.set(school)
        view.findNavController().navigate(SchoolsListFragmentDirections.actionNavigateListToDetails())
    }

}