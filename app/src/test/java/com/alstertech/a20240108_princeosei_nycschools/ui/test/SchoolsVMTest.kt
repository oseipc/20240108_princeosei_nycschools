package com.alstertech.a20240108_princeosei_nycschools.ui.test

import com.alstertech.a20240108_princeosei_nycschools.repos.SchoolsRepository
import com.alstertech.a20240108_princeosei_nycschools.viewmodels.SchoolsVM
import org.junit.Test
import org.mockito.Mockito.mock

/**
 * SchoolsVMTest.kt
 * Demonstration of unit testing for one function in view-model.
 */
class SchoolsVMTest {

    private val schoolsRepo = mock(SchoolsRepository::class.java)

    private val schoolsVM = SchoolsVM(schoolsRepo)

    companion object {
        const val ERROR_MESSAGE_DATA_NOT_FOUND = "data not found"
    }
    @Test
    fun schoolsVM_ShowErrorMessage_MessageNotFoundErrorShownWithMessage() {
        val currentShowSchoolsList = schoolsVM.showSchoolsList.get()
        val currentShowLoadingDataIndicator = schoolsVM.showLoadingDataIndicator.get()
        val currentShowDataNotFoundMessage = schoolsVM.showDataNotFoundMessage.get()
        val currentErrorMessage = schoolsVM.errorMessage.get()

        schoolsVM.showErrorMessage(ERROR_MESSAGE_DATA_NOT_FOUND)

        assert(!schoolsVM.showSchoolsList.get())
        assert(!schoolsVM.showLoadingDataIndicator.get())
        assert(schoolsVM.showDataNotFoundMessage.get())
        assert(schoolsVM.errorMessage.get() == ERROR_MESSAGE_DATA_NOT_FOUND)
        assert(currentErrorMessage != schoolsVM.errorMessage.get())
        if (currentShowSchoolsList) {
            assert(currentShowSchoolsList != schoolsVM.showSchoolsList.get())
        } else {
            assert(currentShowSchoolsList == schoolsVM.showSchoolsList.get())
        }
        assert(currentShowLoadingDataIndicator != schoolsVM.showLoadingDataIndicator.get())
        assert(currentShowDataNotFoundMessage != schoolsVM.showDataNotFoundMessage.get())
    }


}